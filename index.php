<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Aufgabe Verschlüsselung</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
</head>
<body class="bg-light">
<div class="container w-25 p-3 border bg-white">
    <div class="bg-cyan-400">
        <h1 class="fw-light">Aufgabe Verschlüsselung</h1>
    </div>

    <form method="get" action="../index.php" class="row g-3">
        <label for="operation" class="bg-light fs-6">Enter you operation</label>
        <input type="text" id="operation" class="form-control " placeholder="Operation" name="operation">
        <input type="text" id="operation" class="form-control " placeholder="Key" name="key">
        <div class="col-auto">
            <button type="submit" class="btn btn-sm btn-primary"> verschlüssel</button>
        </div>
    </form>

    <?php

    if (isset($_GET['operation']) && isset($_GET['key'])) {

        require_once("CaesarEncryption.php");

        $encryption = new CaesarEncryption();

        $encodedStr = $encryption->encrypt($_GET['operation'], $_GET['key']);

        $decryptStr = $encryption->decrypt($encodedStr, $_GET['key']);

       echo $encodedStr . "<br>";
       echo $decryptStr;
    }
    ?>
    </div>
    </body>
</html>