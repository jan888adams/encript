<?php

class CaesarEncryption
{

    public function encrypt(string $str, int $k): string
    {
        $str = strtoupper($str);
        $str = $this->replaceSpecialChars($str);

        $arrayStr = str_split($str, 1);

        $encryptedArray = [];

        for ($i = 0; $i < count($arrayStr); $i++) {

            $number = ord($arrayStr[$i]) + $k;

            if ($number > 90) {
                $difference = $number - 90;
                $number = 65 + $difference;
            }

            //   var_dump($number);

            $encryptedArray[] = chr($number);

        }
        return implode($encryptedArray);

    }

    public function decrypt(string $str, int $k): string
    {
        $reverseKey = $k * -1;
        return $this->encrypt($str, $reverseKey);
    }

    private function replaceSpecialChars($str)
    {
        $str = str_replace("Ä", "AE", $str);
        $str = str_replace("Ü", "UE", $str);
        $str = str_replace("Ö", "OE", $str);
        $str = str_replace("ß", "SS", $str);

        return $str;
    }
  // preg_split("//")
}