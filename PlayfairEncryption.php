<?php

class PlayfairEncryption
{
    private $ascRepresentation;

    /**
     * PlayFairEncryption constructor.
     */
    public function __construct()
    {
        $this->ascRepresentation = 64; // A = 65 <-> Z = 90
    }

    /**
     * @param $key
     * @param $text
     * @return string
     */
    public function encrypt(string $key, string $text): string
    {

        $playfairSquare = $this->preparePlayFairSquare($key);

        echo "gatlmzclrqtx";

        return implode($this->transformText($playfairSquare, $text, 1));

    }

    public function decrypt(string $key, string $text): string
    {

        $playfairSquare = $this->preparePlayFairSquare($key);

        return implode($this->transformText($playfairSquare, $text, -1));
    }

    private function transformText(Array $playfairSquare, string $text, int $moveDirection): array
    {

        $text = strtoupper($text);
        $textAsArray = str_split($text, 1);

        $textAsArray = $this->replaceDuplicateChars($textAsArray);
        // var_dump($textWhitReplacedChars);

        $encryptedText = [];

        // var_dump($textWhitReplacedChars);

        if ($textAsArray % 2 == 1 && $moveDirection == 1) {
            $textAsArray[] = "Z";
        }

        var_dump(implode($textAsArray));
        var_dump(count($textAsArray));

        for ($i = 0; $i < count($textAsArray); $i += 2) {

            $a = $textAsArray[$i];
            $b = $textAsArray[$i + 1];

            $positionA = $this->getPosition($playfairSquare, $a);
            $positionB = $this->getPosition($playfairSquare, $b);

            if ($positionA[1] == $positionB[1]) {

                $newYPositionA = $positionA[0] + $moveDirection;

                if ($newYPositionA > 4) {
                    $newYPositionA = 0;
                }

                if($newYPositionA < 0){
                    $newYPositionA = 4;
                }

                $encryptedText[] = $playfairSquare[$newYPositionA][$positionA[1]];

                $newYPositionB = $positionB[0] + $moveDirection;

                if ($newYPositionB > 4) {
                    $newYPositionB = 0;
                }

                if($newYPositionB < 0){
                    $newYPositionB = 4;
                }

                $encryptedText[] = $playfairSquare[$newYPositionB][$positionB[1]];

            } else if ($positionA[0] == $positionB[0]) {

                $newXPositionA = $positionA[1] + $moveDirection;

                if ($newXPositionA > 4) {
                    $newXPositionA = 0;
                }

                if($newXPositionA < 0){
                    $newXPositionA = 4;
                }

                $encryptedText[] = $playfairSquare[$positionA[0]][$newXPositionA];
                $newXPositionB = $positionB[1] + 1;

                if ($newXPositionB > 4) {
                    $newXPositionB = 0;
                }

                if($newXPositionB < 0){
                    $newXPositionB = 4;
                }

                $encryptedText[] = $playfairSquare[$positionB[0]][$newXPositionB];

            } else {

                $newXPositionA = $positionB[1];
                $newXPositionB = $positionA[1];
                $encryptedText[] = $playfairSquare[$positionA[0]][$newXPositionA];
                $encryptedText[] = $playfairSquare[$positionB[0]][$newXPositionB];

            }
        }

        return $encryptedText;

    }



    private function preparePlayFairSquare(string $key){

        $key = strtoupper($key);
        $keyAsArray = str_split($key, 1);
        $keyAsArray = $this->replaceDoubleLettersInKey($keyAsArray);

        $playfairSquare = [];

        $row1 = array_fill(0, 5, "");
        $row2 = array_fill(0, 5, "");
        $row3 = array_fill(0, 5, "");
        $row4 = array_fill(0, 5, "");
        $row5 = array_fill(0, 5, "");

        $playfairSquare[0] = $row1;
        $playfairSquare[1] = $row2;
        $playfairSquare[2] = $row3;
        $playfairSquare[3] = $row4;
        $playfairSquare[4] = $row5;

        $playfairSquare = $this->fillWithKey($keyAsArray, $playfairSquare);

        return $this->fillEmptySpots($keyAsArray, $playfairSquare);
    }

    private function fillEmptySpots($keyAsArray, $playfairSquare){
        for ($i = 0; $i < count($playfairSquare); $i++) {
            $playfairSquare[$i] = $this->fillWithLetters($keyAsArray, $playfairSquare[$i]);
        }

        return $playfairSquare;
    }

    public function replaceDoubleLettersInKey($arrayKey): array
    {
        $arrayKeyWithoutDoubleLetters = [];

        for ($i = 0; $i < count($arrayKey); $i++) {

            if ($arrayKey[$i] == "J") {
                $arrayKey[$i] = "L";
            }

            if (in_array($arrayKey[$i], $arrayKeyWithoutDoubleLetters)) {

                $newLetter = chr($this->ascRepresentation);

                while (in_array($newLetter, $arrayKeyWithoutDoubleLetters)) {
                    $newLetter = chr($this->nextAscRepresentation());
                }

                $arrayKeyWithoutDoubleLetters[] = $newLetter;

            } else {
                $arrayKeyWithoutDoubleLetters[] = $arrayKey[$i];
            }
        }

        return $arrayKeyWithoutDoubleLetters;

    }

    public function nextAscRepresentation(): int
    {
        if ($this->ascRepresentation + 1 > 90) {
            $this->ascRepresentation = 65;
        } else {
            $this->ascRepresentation += 1;
        }

        return $this->ascRepresentation;
    }

    public function getPosition($playFairSquare, $a): array
    {
        $position = [];

        for ($i = 0; $i < count($playFairSquare); $i++) {

            for ($e = 0; $e < count($playFairSquare[$i]); $e++) {
                if ($playFairSquare[$i][$e] == $a) {
                    $position[0] = $i;
                    $position[1] = $e;
                }
            }
        }

        return $position;
    }

    public function fillWithLetters($arrayKey, $row)
    {

        for ($i = 0; $i < 5; $i++) {

            if ($row[$i] == "") {

                $letter = $this->nextAscRepresentation();

                while (in_array(chr($letter), $arrayKey)) {
                    $letter = $this->nextAscRepresentation();
                }

                // var_dump(chr($this->letterAsNumber));

                if ($letter == 74) {
                    $letter = $this->nextAscRepresentation();
                }

                $row[$i] = chr($letter);
            }

        }

        return $row;

    }

    public function fillWithKey($arrayKey, $playFairSquare)
    {
        for ($i = 0; $i < count($playFairSquare); $i++) {

            if (count($arrayKey) > 5) {
                $playFairSquare[$i] = $this->fill(array_slice($arrayKey, 0, 5), $playFairSquare[$i]);
                array_splice($arrayKey, 0, 5);
                //var_dump($arrayKey);
            } else {
                $playFairSquare[$i] = $this->fill($arrayKey, $playFairSquare[$i]);
                break;
            }
        }

        return $playFairSquare;
    }

    private function fill($arrayKey, $row)
    {
        for ($i = 0; $i < count($arrayKey); $i++) {
            $row[$i] = $arrayKey[$i];
        }

        return $row;
    }

    public function replaceDuplicateChars($array)
    {
        $lastLetter = "";

        for ($i = 0; $i < count($array); $i++) {

            if ($i > 0) {
                $lastLetter = $array[$i - 1];
            }
            if ($array[$i] == $lastLetter) {
                array_splice($array, $i, 0, "X");
            }

        }

        return $array;
    }

}